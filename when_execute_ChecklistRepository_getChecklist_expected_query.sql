
select spc.id,
       spc.created_at,
       spc.created_by,
       spc.modified_at,
       spc.modified_by,
       spc.version,
       spc.can_upload_rules,
       spc.checklist_id,
       spc.mandatory,
       spc.mandatory_only_by_rules,
       spc.mandatory_rules,
       spc.product_id,
       spc.stage_id
from stage_product_checklist spc
         inner join stage s on spc.stage_id = s.id
         left outer join product p on spc.product_id = p.id
         inner join checklist c on spc.checklist_id = c.id
         inner join participant_type pt on c.participant_type_id = pt.id
where c.context = 'DECISION'
  and (
    p.sublimit_code = 'MLP'
        and p.product_family_code = '301'
        and p.product_code = '204'
        or spc.product_id is null
    )
  and (pt.code in ('CLIENT', 'CLIENT_REPRESENTATIVES_AND_GUARANTORS', 'SGM_EGL', 'OTHERS'));
